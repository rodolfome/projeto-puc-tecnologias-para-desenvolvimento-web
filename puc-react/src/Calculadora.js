import React from 'react';

import './Calculadora.css';

const Resultado = (props) => {
	return (
		<div className='resultado'>
			{props.valor}
		</div>
	);
}


const Botao = (props) => {
	return (
		<div className={`digito ${props.modo}`} style={{ maxWidth: `${(props.space * 25)}%` }}>
			{props.valor}
		</div>
	);
}



function Calculadora() {
	return (
		<div className='calculadora'>
			<div className='linha'>
				<Resultado valor={0.75} />
			</div>
			
			<div className='linha'>
				<Botao space={3} valor="AC" />
				<Botao space={1} modo="operacao" valor="/" />
			</div>

			<div className='linha'>
				<Botao space={1} valor="7" />
				<Botao space={1} valor="8" />
				<Botao space={1} valor="9" />
				<Botao space={1} modo="operacao" valor="*" />
			</div>

			<div className='linha'>
				<Botao space={1} valor="4" />
				<Botao space={1} valor="5" />
				<Botao space={1} valor="6" />
				<Botao space={1} modo="operacao" valor="-" />
			</div>

			<div className='linha'>
				<Botao space={1} valor="1" />
				<Botao space={1} valor="2" />
				<Botao space={1} valor="3" />
				<Botao space={1} modo="operacao" valor="+" />
			</div>

			<div className='linha'>
				<Botao space={2} valor="0" />
				<Botao space={1} valor="." />			
				<Botao space={1} modo="operacao" valor="=" />
			</div>
		</div>
	);
}

export default Calculadora;