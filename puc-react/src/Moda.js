import React, { useMemo } from 'react';


function Moda() {

	const resultadoModa = useMemo(() => {
		let numeroVezes;
		let numeroVezesAnterior = 0;
		let moda = 0;
		let vetor = [ 5, 5, 2, 3, 1, 3, 8 ];

		for (let i = 0; i < vetor.length; i++)
		{
			numeroVezes = 1;

			for(var j = 0; j < vetor.length; j++)
			{				
				if (vetor[i] === vetor[j])
					numeroVezes++;
			}

			if (numeroVezes > numeroVezesAnterior) {
				moda = vetor[i];
				numeroVezesAnterior = numeroVezes;
			}
		}

		console.log(`Moda: ${moda}`);
		return moda;
	}, []);

  	return (
		<div style={{ marginTop: 15 }}>			
			<p style={{ padding: 10 }}>Moda = {resultadoModa}</p>
		</div>
	);
}

export default Moda;