import React, { Component } from 'react';

import './App.css';
import Calculadora from './Calculadora';
import Moda from './Moda';

class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			email: "",
			senha: "",
			logado: null,
		}

		this.login = this.login.bind(this);
	}


	mudarEmail(valor) {
		this.setState({ email: valor.currentTarget.value });
	}

	mudarSenha(valor) {
		this.setState({ senha: valor.currentTarget.value });
	}


	login() {	
		if (this.state.email === "rodolfo@email.com" && this.state.senha === "112233")
			this.setState({ logado: true });
		else
			this.setState({ logado: false });
	}



	render() {
		return (
			<div className="App">
				<div className="login">
					<h1>Login</h1>
	
					<div className="form-row">
						<label>E-mail</label>
						<input type="email" onChange={(e) => this.mudarEmail(e)} />
					</div>
	
					<div className="form-row">
						<label>Senha</label>
						<input type="password" onChange={(e) => this.mudarSenha(e)} />
					</div>
	
					<div className="acoes">					
						<button onClick={this.login}>Acessar</button>
					</div>

					{this.state.logado !== null && (
						<div className={`alert ${this.state.logado === true ? `success` : `error`}`}>
							{this.state.logado === true ? `Acessado com sucesso!` : `Usuário ou senha incorretos!`}
						</div>
					)}	

					<hr style={{ marginTop: 35, marginBottom: 10 }} />
					<p style={{ color: '#555' }}>
						Exercícios das semanas anteriores
					</p>
					<Moda />
					
					<h1>Calculadora</h1>
					<Calculadora />				
				</div>
			</div>
		);
	}
	
}

export default App;
